﻿Imports System.ComponentModel
Imports System.IO
Imports Microsoft.Win32

Partial Public Class EncoderTest

    Private save As New SaveFileDialog With { _
        .CreatePrompt = True, _
        .FileName = "", _
        .Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*", _
        .InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments}
    Private open As New OpenFileDialog With { _
        .FileName = "", _
        .Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*", _
        .InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments, _
        .CheckFileExists = True}

#Region "Encode"

    Private Sub EncodeButton_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles EncodeButton.Click
        Dim p1 As New List(Of String)(WordsTextBox.Text.Split( _
            New String() {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries))
        For Each cont As UIElement In MainGrid.Children
            cont.IsEnabled = False
        Next
        WordsTextBox.Text = "Encoding text..."
        encodeBW.RunWorkerAsync(p1)
    End Sub

    Private WithEvents encodeBW As New BackgroundWorker

    Private Sub encodeBW_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles encodeBW.DoWork
        Dim p1 As List(Of String) = CType(e.Argument, List(Of String))

        e.Result = BookwormListEncoder.Encode(p1)
    End Sub

    Private Sub encodeBW_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles encodeBW.RunWorkerCompleted
        Try
            WordsTextBox.Text = String.Join(Environment.NewLine, _
                                            CType(e.Result, List(Of String)).ToArray)
        Catch
            MessageBox.Show(e.Error.Message, e.Error.GetType.ToString, MessageBoxButton.OK, MessageBoxImage.Error)
        End Try
        For Each cont As UIElement In MainGrid.Children
            cont.IsEnabled = True
        Next
    End Sub

#End Region

#Region "Decode"

    Private Sub DecodeButton_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles DecodeButton.Click
        Dim p1 As New List(Of String)(WordsTextBox.Text.Split( _
            New String() {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries))
        For Each cont As UIElement In MainGrid.Children
            cont.IsEnabled = False
        Next
        WordsTextBox.Text = "Decoding text..."
        decodeBW.RunWorkerAsync(p1)
    End Sub

    Private WithEvents decodeBW As New BackgroundWorker

    Private Sub decodeBW_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles decodeBW.DoWork
        e.Result = New List(Of String)
        Dim p1 As List(Of String) = CType(e.Argument, List(Of String))

        Try
            e.Result = BookwormListEncoder.Decode(p1)
        Catch ex As Exception
            MessageBox.Show(ex.Message, _
                            ex.GetType.ToString & " thrown", _
                            MessageBoxButton.OK, _
                            MessageBoxImage.Error)
            e.Result = p1
        End Try
    End Sub

    Private Sub decodeBW_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles decodeBW.RunWorkerCompleted
        WordsTextBox.Text = String.Join(Environment.NewLine, _
                                    CType(e.Result, List(Of String)).ToArray)
        For Each cont As UIElement In MainGrid.Children
            cont.IsEnabled = True
        Next
    End Sub

#End Region

#Region "Save"

    Private WithEvents saveBW As New BackgroundWorker With _
        {.WorkerReportsProgress = True}
    Private saveTmp As String

    <Security.Permissions.FileDialogPermission(Security.Permissions.SecurityAction.Assert)> _
    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles SaveButton.Click
        saveTmp = WordsTextBox.Text
        For Each cont As UIElement In MainGrid.Children
            cont.IsEnabled = False
        Next
        saveBW.RunWorkerAsync(WordsTextBox.Text)
    End Sub

    Private Sub saveBW_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles saveBW.DoWork
        If Not save.ShowDialog() Then Return
        Dim s As String = CType(e.Result, String)
        Using fs = File.OpenWrite(save.FileName)
            Using ts As New StreamWriter(fs)
                Dim i As Integer
                For Each c As Char In s
                    If i Mod 10 = 0 Then
                        Try
                            saveBW.ReportProgress(Math.Round(i / s.Length * 100))
                        Catch ex As NotFiniteNumberException
                        Catch ex As DivideByZeroException
                        End Try
                    End If
                    ts.Write(c)
                    i += 1
                Next
            End Using
        End Using
    End Sub

    Private Sub saveBW_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles saveBW.ProgressChanged
        WordsTextBox.Text = String.Format("Saving... {0}%", e.ProgressPercentage)
    End Sub

    Private Sub saveBW_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles saveBW.RunWorkerCompleted
        WordsTextBox.Text = saveTmp
        For Each cont As UIElement In MainGrid.Children
            cont.IsEnabled = True
        Next
    End Sub

#End Region

#Region "Open"

    <Security.Permissions.FileDialogPermission(Security.Permissions.SecurityAction.Assert)> _
    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles OpenButton.Click
        If Not open.ShowDialog() Then Return
        For Each cont As UIElement In MainGrid.Children
            cont.IsEnabled = False
        Next
        fileSize = (New FileInfo(open.FileName)).Length
        openBW.RunWorkerAsync()
    End Sub

    Private WithEvents openBW As New BackgroundWorker With _
        {.WorkerReportsProgress = True}
    Private fileSize As Long

    Private Sub openBW_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles openBW.DoWork
        Dim ret As New Text.StringBuilder

        Using fs As FileStream = File.OpenRead(open.FileName)
            Using ts As New StreamReader(fs)
                Dim i As Long
                Do Until ts.EndOfStream
                    If i Mod 256 = 0 Then
                        openBW.ReportProgress(0, i)
                    End If
                    ret.Append(ChrW(ts.Read))
                    i += 1
                Loop
            End Using
        End Using
        e.Result = ret.ToString
    End Sub

    Private Sub openBW_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles openBW.ProgressChanged
        WordsTextBox.Text = String.Format("Opening... ~{0} of {1}B", CLng(e.UserState), fileSize)
    End Sub

    Private Sub openBW_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles openBW.RunWorkerCompleted
        WordsTextBox.Text = CType(e.Result, String)
        For Each cont As UIElement In MainGrid.Children
            cont.IsEnabled = True
        Next
    End Sub

#End Region

End Class
