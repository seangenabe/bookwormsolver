﻿
Imports System
Imports System.IO

''' <summary>
''' Converts PopCap's Bookworm Deluxe-style word lists to and from manageable word lists.
''' </summary>
''' <remarks></remarks>
Public NotInheritable Class BookwormListEncoder

    ''' <summary>
    ''' Converts the encoded word list to a word list.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Decode(ByVal wordList As List(Of String)) As List(Of String)
        Dim currentWord As String = wordList(0)
        Dim line As String = ""
        Dim numberPref As Integer       ' The prefix of the word
        Dim numberPrefLast As Integer   ' The prefix of the last word.
        Dim ret As New List(Of String)
        Dim numberLen As Integer
        Dim nTmp As Integer
        Dim digitLength As Integer      ' The length of a digit in the prefix.
        Dim hasPrefix As Boolean = False

        ret.Add(currentWord)
        For Each line In wordList.Skip(1)
            ' Determine the number prefix
            For numberLen = 0 To line.Length - 1
                If Not Integer.TryParse(line(numberLen), nTmp) Then
                    Exit For
                End If
            Next
            If numberLen = 0 Then
                ' Prefix was not found, inherit prefix from last word
                numberPref = numberPrefLast
                hasPrefix = False
            Else
                numberPref = Integer.Parse(line.Substring(0, numberLen))
                hasPrefix = True
            End If

            ' Process
            digitLength = IIf(hasPrefix, numberPref.ToString( _
                Globalization.CultureInfo.InvariantCulture.NumberFormat).Length, _
                0)
            Dim newWord1 As String = currentWord.Substring(0, numberPref)
            Dim newWord2 As String = line.Substring(digitLength, line.Length - digitLength)
            currentWord = newWord1 & newWord2
            ret.Add(currentWord)

            numberPrefLast = numberPref
        Next
        Return ret
    End Function

    ''' <summary>
    ''' Encodes the word list.
    ''' </summary>
    ''' <param name="wordList"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function Encode(ByVal wordList As List(Of String)) As List(Of String)
        wordList.Sort()
        Dim ret As New List(Of String)
        Dim letterPref As Integer
        Dim letterPrefLast As Integer
        Dim lastLine As String = wordList(0)
        If wordList.Count = 0 Then Return ret

        ret.Add(lastLine)
        For Each line In wordList.Skip(1)
            For letterPref = 0 To line.Length - 1
                If lastLine.Length < letterPref + 1 Then Exit For
                If Not lastLine(letterPref).Equals(line(letterPref)) Then Exit For
            Next

            ret.Add(String.Format("{0}{1}", _
                IIf(letterPref <> letterPrefLast, _
                    letterPref.ToString( _
                        Globalization.CultureInfo.InvariantCulture.NumberFormat), _
                    ""), _
                line.Substring(letterPref, line.Length - letterPref)))

            lastLine = line
            letterPrefLast = letterPref
        Next
        Return ret
    End Function

End Class
