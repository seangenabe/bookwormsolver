﻿Imports System
Imports System.Collections.ObjectModel
Imports System.ComponentModel
Imports System.Globalization
Imports Microsoft.Win32
Imports System.IO

Class Window1

    Private wordList As New List(Of String)
    Private textOpenFileDialog As New OpenFileDialog With { _
        .Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*", _
        .InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments}

    Private Sub Window1_Loaded(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles MyBase.Loaded
        ' LoadWordMaster()
        StartLoadWords()
    End Sub

    Private WithEvents loadingWindow1 As New LoadingWindow

    ''' <summary>
    ''' Starts loading the word list by showing a loading window and loading
    ''' the words in a separate thread.
    ''' </summary>
    Friend Sub StartLoadWords()
        IsEnabled = False
        loadingWindow1.Show()
        loadingWindow1.LoadWords()
    End Sub

    Private Sub loadingWindow1_LoadingFinished(ByVal words As System.Collections.Generic.List(Of String), ByVal doExit As Boolean) Handles loadingWindow1.LoadingFinished
        wordList = words
        IsEnabled = True
        loadingWindow1.Close()
        LoadWordsToWordFilter()
        LoadLetterRip()
    End Sub

    Private Sub OpenFile()
        Dim curLine As String
        If textOpenFileDialog.ShowDialog Then
            wordList.Clear()
            Using s As New StreamReader(textOpenFileDialog.FileName)
                curLine = s.ReadLine
                If Not curLine.Contains(" ") Then wordList.Add(curLine)
            End Using
        End If
    End Sub

    Private Sub OpenEncodedMenuItem_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles OpenEncodedMenuItem.Click
        'Dim tmpWordList
        Dim curLine As String 
        If textOpenFileDialog.ShowDialog Then
            wordList.Clear()
            Using s As New StreamReader(textOpenFileDialog.FileName)
                curLine = s.ReadLine
                If Not curLine.Contains(" ") Then wordList.Add(curLine)
            End Using
        End If
    End Sub

    Private Sub Refresh(ByVal sender As Object, ByVal e As ExecutedRoutedEventArgs)
        CheckRef()
        LrRefresh()
    End Sub

    Private Sub CanRefresh(ByVal sender As Object, ByVal e As CanExecuteRoutedEventArgs)
        e.CanExecute = RefreshCheckMenuItem.IsChecked
    End Sub

#Region "Word Master"

    Private _selectedLetter As Button
    Private _letterStatDic As New Dictionary(Of Button, ButtonStat)
    Private _letterWord(5, 5) As Char
    Private Buttons As New List(Of List(Of Button))
    Private WordChecks As New List(Of CheckBox)

    Private _wordIndexDic As New Dictionary(Of Button, Integer)
    Private _letterIndexDic As New Dictionary(Of Button, Integer)

    Private Enum ButtonStat As Integer
        None = 0
        Misplaced = 1
        Correct = 2
    End Enum

    Public ReadOnly Property Words() As List(Of String)
        Get
            Return wordList
        End Get
    End Property

    Private Sub LoadWordMaster()
        ' Load buttons
        For Each x As Button In (From a In Me.UniformGrid1.Children _
                       Where TypeOf a Is Button Select a)
            _letterStatDic.Add(x, ButtonStat.None)
            AddHandler x.Click, AddressOf Letter_Click
            AddHandler x.KeyDown, AddressOf Letter_KeyDown
            AddHandler x.GotFocus, AddressOf Letter_GotFocus
            x.Background = Nothing
        Next
        Word1Letter1.Focus()

        For wordIndex0 = 0 To 4
            Dim wordIndex = wordIndex0
            Dim word As New List(Of Button)
            word.AddRange((From a In Me.UniformGrid1.Children _
                Where TypeOf a Is Button AndAlso Val(a.Name(4)) = wordIndex + 1 _
                Select a Order By CType(a, Button).Name).Cast(Of Button))
            For j = 0 To word.Count - 1
                _letterIndexDic.Add(word(j), j)
                _wordIndexDic.Add(word(j), wordIndex)
            Next
            Buttons.Add(word)
        Next

        ' Load checkboxes
        WordChecks.AddRange((From a In Me.UniformGrid1.Children _
                Where TypeOf a Is CheckBox Select a _
                Order By CType(a, CheckBox).Name).Cast(Of CheckBox))
        For Each x In WordChecks
            AddHandler x.Checked, AddressOf Checks_Checked
            AddHandler x.Unchecked, AddressOf Checks_Unchecked
        Next

        WordsListBox.ItemsSource = results
    End Sub

    Private Sub Reset()
        For Each x In _letterStatDic
            _letterStatDic(x.Key) = ButtonStat.None
            RepaintLetter(x.Key)
        Next
        SelectedLetter = Word1Letter1
        results.Clear()
    End Sub

    Private Sub Letter_GotFocus(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Letter_Click2(sender, e)
    End Sub

    Private Sub Letter_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs)
        Letter_Click2(sender, e)
    End Sub

    Private Sub Letter_Click2(ByVal sender As Object, ByVal e As RoutedEventArgs)
        Dim button As Button = CType(sender, Button)
        If button.Equals(SelectedLetter) Then
            ' Toggle button stat
            _letterStatDic(button) = (_letterStatDic(button) + 1) Mod 3
            RepaintLetter(button)
        Else
            SelectedLetter = button
        End If
    End Sub

    Private Sub Letter_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Input.KeyEventArgs)
        Dim button As Button = CType(sender, Button)
        Dim wordIndex As Integer = _wordIndexDic(button)
        Dim letterIndex As Integer = _letterIndexDic(button)

        If e.Key >= 44 AndAlso e.Key <= 69 Then
            Dim c As Char = ChrW(e.Key - 44 + AscW("A"c))
            _letterWord(wordIndex, letterIndex) = c
            button.Content = c
            ' Select next letter
            If letterIndex = 4 Then
                Dim b = Buttons((wordIndex + 1) Mod 5)(0)
                b.Focus()
            Else
                Dim b = Buttons(wordIndex)(letterIndex + 1)
                b.Focus()
            End If
        ElseIf e.Key = Key.Back AndAlso Not letterIndex = 0 Then
            ' Backspace
            Dim lastLetter = Buttons(wordIndex)(letterIndex - 1)
            lastLetter.Focus()
            _letterWord(wordIndex, letterIndex) = Nothing
            SelectedLetter.Content = "*"
        ElseIf e.Key = Key.Delete Then
            _letterWord(wordIndex, letterIndex) = Nothing
            button.Content = "*"
        End If
    End Sub

    Private Property SelectedLetter() As Button
        Get
            Return _selectedLetter
        End Get
        Set(ByVal value As Button)
            Dim prevLetter = _selectedLetter
            _selectedLetter = value
            If Not IsNothing(prevLetter) Then RepaintLetter(prevLetter)
            RepaintLetter(value)
        End Set
    End Property

    Private Sub RepaintLetter(ByVal letterButton As Button)
        Dim bg As Brush = Brushes.Transparent
        If Equals(SelectedLetter, letterButton) Then
            Select Case _letterStatDic(letterButton)
                Case ButtonStat.None
                    bg = Resources("SelectedBrush")
                Case ButtonStat.Misplaced
                    bg = Resources("SelectedMisplacedBrush")
                Case ButtonStat.Correct
                    bg = Resources("SelectedCorrectBrush")
            End Select
        Else
            Select Case _letterStatDic(letterButton)
                Case ButtonStat.None
                    bg = Nothing
                Case ButtonStat.Misplaced
                    bg = Resources("MisplacedBrush")
                Case ButtonStat.Correct
                    bg = Resources("CorrectBrush")
            End Select
        End If
        letterButton.Background = bg
    End Sub

    Private Sub ExitMenuItem_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles ExitMenuItem.Click
        Close()
    End Sub

    Private Sub WordsEncoderMenuItem_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles WordsEncoderMenuItem.Click
        Dim et As New EncoderTest
        et.Show()
    End Sub

    Private Sub Checks_Checked(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs)
        Dim ch As CheckBox = CType(sender, CheckBox)
        Dim cIndex As Integer = WordChecks.IndexOf(ch)
        If Not cIndex = 0 Then WordChecks(cIndex - 1).IsChecked = True
        Checked_Changed()
    End Sub

    Private Sub Checks_Unchecked(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs)
        Dim ch As CheckBox = CType(sender, CheckBox)
        Dim cIndex As Integer = WordChecks.IndexOf(ch)
        If Not cIndex = WordChecks.Count - 1 Then WordChecks(cIndex + 1).IsChecked = False
        Checked_Changed()
    End Sub

    Private Function GetWords() As List(Of String)
        Dim ret As New List(Of String)
        For i As Integer = 0 To WordChecks.Count - 1
            If WordChecks(i).IsChecked Then
                Dim chars(4) As Char
                For j = 0 To 4
                    chars(j) = _letterWord(i, j)
                Next
                If chars.Any(Function(x) IsNothing(x)) Then Continue For
                ret.Add(New String(chars).ToLower)
            Else
                Exit For
            End If
        Next
        Return ret
    End Function

    Private Sub Checked_Changed()
        Dim words As List(Of String) = GetWords()
        If RecalculateCheckBox.IsChecked Then Recalculate()
    End Sub

    Private Sub ResetButton_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles ResetButton.Click
        Reset()
    End Sub

    Private Sub RecaculateButton_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles RecaculateButton.Click
        Recalculate()
    End Sub

    Dim results As New List(Of String)
    Dim excludedLetters As New List(Of Char)    ' Refreshed on recalculate

    Private Sub Recalculate()
        Dim cIndex As Integer = WordChecks.IndexOf(WordChecks.First(Function(x) x.IsChecked))
        Dim getWords As List(Of String) = Me.GetWords()

        If getWords.Count - 1 <> cIndex Then
            MessageBox.Show("Some letters are left blank. Please fill in the missing letters.", _
                            "Error - blank letters", _
                            MessageBoxButton.OK, MessageBoxImage.Error)
            Return
        End If

        ' Refresh excluded letters
        excludedLetters.Clear()
        For i = 0 To getWords.Count - 2
            For j = 0 To 4
                Dim b = Buttons(i)(j)
                If _letterStatDic(b) = ButtonStat.None Then
                    excludedLetters.Add(_letterWord(i, j))
                End If
            Next
        Next

        Dim res As List(Of String)

        ' Determine last checked
        Dim lastChecked As Integer
        For lastChecked = 4 To 0 Step -1
            If WordChecks(lastChecked).IsChecked Then Exit For
        Next

        If lastChecked > cIndex Then
            If FilterCheckBox.IsChecked Then
                If results.Count = 0 Then res = wordList Else res = results

                For i = lastChecked + 1 To cIndex
                    res = From x In res Where PossibleWord(x) Select x
                Next
            Else
                res = wordList

                For i = 0 To cIndex
                    res = From x In res Where PossibleWord(x) Select x
                Next
            End If
        Else
            res = wordList

            For i = 0 To cIndex
                res = New List(Of String)(From x In res Where PossibleWord(x) Select x)
            Next
        End If

        results = res
    End Sub

    Private Function PossibleWord(ByVal word As String) As Boolean

        Dim words As List(Of String) = GetWords()
        For i0 As Integer = 0 To 4
            Dim i = i0
            If (From x In _letterIndexDic _
                    Where _letterIndexDic(x.Key) = i Select Buttons(x.Value)(i)) _
                        .Any(Function(y) _letterStatDic(y) = ButtonStat.Correct) Then
                ' If any button (in the column 'i') is correct then...
            Else
                Return False
            End If
        Next

        ' Determine misplaced letters
        Dim misplacedLetters As New List(Of Char)
        For i = 0 To words.Count - 1
            For j = 0 To 4
                If _letterStatDic(Buttons(i)(j)) = ButtonStat.Misplaced Then
                    misplacedLetters.Add(_letterWord(i, j))
                ElseIf _letterStatDic(Buttons(i)(j)) = ButtonStat.Correct Then
                    misplacedLetters.Remove(_letterWord(i, j))
                End If
            Next
        Next

        ' If some misplaced letters are in the correct positions, exclude it
        For i0 = 0 To 4
            Dim i = i0
            If (From x In _letterIndexDic _
                    Where _letterIndexDic(x.Key) = i Select Buttons(x.Value)(i)) _
                    .Any(Function(y) misplacedLetters.Contains( _
                        _letterWord(_wordIndexDic(y), _letterIndexDic(y)))) Then
                Return False
            End If
        Next

        ' If the word contains any excluded letters, exclude it
        If word.Intersect(excludedLetters).Any Then
            Return False
        End If

        ' The word is qualified
        Return True
    End Function

#End Region

#Region "Word Filter"

    Dim wordList2 As New List(Of String)
    Dim rSort As Boolean = False

    Private Sub LoadWordsToWordFilter()
        wordList2 = New List(Of String)(wordList)
        WordFilterListBox.ItemsSource = wordList2
        WordFilterListBox.Items.Filter = AddressOf Filter
    End Sub

    Private Sub WordLengthSlider1_ValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of System.Double)) Handles WordLengthSlider1.ValueChanged
        If WordLengthLockCheckBox.IsChecked Then
            WordLengthSlider2.Value = WordLengthSlider1.Value
            RangeTextBox.Content = CInt(WordLengthSlider1.Value)
        ElseIf WordLengthSlider1.Value > WordLengthSlider2.Value Then
            WordLengthSlider2.Value = WordLengthSlider1.Value
            RangeTextBox.Content = String.Format("{0} to {1}", WordLengthSlider1.Value, WordLengthSlider2.Value)
        Else
            RangeTextBox.Content = String.Format("{0} to {1}", WordLengthSlider1.Value, WordLengthSlider2.Value)
        End If

        If Not RefreshCheckMenuItem.IsChecked Then
            CheckRef()
        End If
    End Sub

    Private Sub WordLengthSlider2_ValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of System.Double)) Handles WordLengthSlider2.ValueChanged
        If IsNothing(WordLengthLockCheckBox) Then
        ElseIf WordLengthLockCheckBox.IsChecked Then
            WordLengthSlider1.Value = WordLengthSlider2.Value
            RangeTextBox.Content = CInt(WordLengthSlider2.Value)
            Return
        ElseIf WordLengthSlider1.Value > WordLengthSlider2.Value Then
            WordLengthSlider1.Value = WordLengthSlider2.Value
        End If
        If Not IsNothing(RangeTextBox) Then
            RangeTextBox.Content = String.Format("{0} to {1}", WordLengthSlider1.Value, WordLengthSlider2.Value)
        End If

        If Not RefreshCheckMenuItem.IsChecked Then
            CheckRef()
        End If
    End Sub

    Private Sub WordLengthLockCheckBox_Checked(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles WordLengthLockCheckBox.Checked
        Dim t As Boolean = WordLengthSlider1.Value = WordLengthSlider2.Value
        WordLengthSlider2.Value = WordLengthSlider1.Value

        If Not (t OrElse RefreshCheckMenuItem.IsChecked) Then
            CheckRef()
        End If
    End Sub

    Private Sub CheckRef()
        Try
            If Not WordFilterListBox.Items.NeedsRefresh Then
                WordFilterListBox.Items.Filter = AddressOf Filter
                'WordFilterListBox.Items.Refresh()
            End If

            If rSort Then
                rSort = False
                ChSort()
            End If
        Catch ex As NullReferenceException When WordFilterListBox Is Nothing
        End Try
    End Sub

    Private Sub ChSort()
        If SortAscendingCheckBox.IsChecked Then
            If SortLengthCheckBox.IsChecked Then
                wordList2 = New List(Of String) _
                    (From x In wordList2 Select x _
                     Order By x.Length Ascending)
            Else
                wordList2 = New List(Of String) _
                    (From x In wordList2 Select x _
                     Order By x Ascending)
            End If
        Else
            If SortLengthCheckBox.IsChecked Then
                wordList2 = New List(Of String) _
                    (From x In wordList2 Select x _
                     Order By x.Length Descending)
            Else
                wordList2 = New List(Of String) _
                    (From x In wordList2 Select x _
                     Order By x Descending)
            End If
        End If

        WordFilterListBox.ItemsSource = wordList2
    End Sub

    ''' <summary>
    ''' The predicate used to filter the items in the list.
    ''' </summary>
    Private Function Filter(ByVal item As Object) As Boolean
        Dim s As String = CStr(item)

        ' Word length
        If Not (s.Length >= CInt(WordLengthSlider1.Value) _
                AndAlso s.Length <= CInt(WordLengthSlider2.Value)) Then
            Return False
        End If

        ' Content
        Dim c1 As Boolean
        If Choice1CheckBox.IsChecked Then
            c1 = PContent(Choice1ComboBox.Text, s, Choice1TextBox.Text)
        End If

        If Choice2CheckBox.IsChecked Then
            c1 = c1 AndAlso PContent(Choice2ComboBox.Text, s, Choice2TextBox.Text)
        End If

        If Choice5CheckBox.IsChecked Then
            c1 = c1 AndAlso PContent(Choice5ComboBox.Text, s, Choice5TextBox.Text)
        End If

        If Choice6CheckBox.IsChecked Then
            c1 = c1 AndAlso PContent(Choice6ComboBox.Text, s, Choice6TextBox.Text)
        End If

        If Choice3CheckBox.IsChecked Then
            c1 = c1 OrElse PContent(Choice3ComboBox.Text, s, Choice3TextBox.Text)
        End If

        If Choice4CheckBox.IsChecked Then
            c1 = c1 OrElse PContent(Choice4ComboBox.Text, s, Choice4TextBox.Text)
        End If

        If Not c1 Then Return False

        Return True
    End Function

    Private Function PContent(ByVal choice As String, ByVal text As String, ByVal textParam As String) As Boolean
        Select Case choice
            Case "Starts with"
                Return text.StartsWith(textParam)
            Case "Does not start with"
                Return IIf(textParam.Length = 0, True, Not text.StartsWith(textParam))
            Case "Ends with"
                Return text.EndsWith(textParam)
            Case "Does not start with"
                Return IIf(textParam.Length = 0, True, Not text.EndsWith(textParam))
            Case "Contains"
                Return text.Contains(textParam)
            Case "Does not contain"
                Return IIf(textParam.Length = 0, True, Not text.Contains(textParam))
            Case "Has the letters"
                For Each c In textParam
                    If Not text.Contains(c) Then Return False
                Next
                Return True
            Case "Does not have the letters"
                For Each c In textParam
                    If text.Contains(c) Then Return False
                Next
                Return True
            Case Else
                Return False
        End Select
    End Function

    Private Sub ContentGroupChanged(ByVal sender As Object, ByVal e As RoutedEventArgs) _
        Handles Choice1CheckBox.Checked, Choice1CheckBox.Unchecked, _
            Choice1ComboBox.SelectionChanged, Choice1TextBox.TextChanged, _
            Choice2CheckBox.Checked, Choice2CheckBox.Unchecked, _
            Choice2ComboBox.SelectionChanged, Choice2TextBox.TextChanged, _
            Choice3CheckBox.Checked, Choice3CheckBox.Unchecked, _
            Choice3ComboBox.SelectionChanged, Choice3TextBox.TextChanged, _
            Choice4CheckBox.Checked, Choice4CheckBox.Unchecked, _
            Choice4ComboBox.SelectionChanged, Choice4TextBox.TextChanged, _
            Choice5CheckBox.Checked, Choice5CheckBox.Unchecked, _
            Choice5ComboBox.SelectionChanged, Choice5TextBox.TextChanged, _
            Choice6CheckBox.Checked, Choice6CheckBox.Unchecked, _
            Choice6ComboBox.SelectionChanged, Choice6TextBox.TextChanged
        If Not RefreshCheckMenuItem.IsChecked Then
            CheckRef()
        End If
    End Sub

    Private Sub SortGroupChanged(ByVal sender As Object, ByVal e As RoutedEventArgs) _
        Handles SortAscendingCheckBox.Checked, SortAscendingCheckBox.Unchecked, _
            SortLengthCheckBox.Checked, SortLengthCheckBox.Unchecked
        rSort = True
        If Not RefreshCheckMenuItem.IsChecked Then
            CheckRef()
        End If
    End Sub

#End Region

#Region "Letter Rip"

    Private wordList3 As New List(Of String)
    Private lrToSort As Boolean = False

    Private Sub LoadLetterRip()
        wordList3 = New List(Of String)(wordList)
        LetterRipListBox.ItemsSource = wordList3
    End Sub

    Private Sub LrTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.Windows.Controls.TextChangedEventArgs) Handles LrTextBox.TextChanged
        If Not RefreshCheckMenuItem.IsChecked Then
            LrRefresh()
        End If
    End Sub

    Private Sub LrCheckBox_Checked(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) _
            Handles LrSortLengthCheckBox.Checked, LrSortLengthCheckBox.Unchecked, _
            LrSortAscendingCheckBox.Checked, LrSortAscendingCheckBox.Unchecked
        lrToSort = True
        If Not RefreshCheckMenuItem.IsChecked Then
            LrRefresh()
        End If
    End Sub

    Private Sub GroupChanged(ByVal sender As Object, ByVal e As RoutedEventArgs) _
            Handles LrGroupCheckBox.Checked, LrGroupCheckBox.Unchecked
        LrGroup()
    End Sub

    Private Sub LrRefresh()
        Try
            If Not LetterRipListBox.Items.NeedsRefresh Then
                LetterRipListBox.Items.Filter = AddressOf LrFilter
            End If

            If lrToSort Then
                lrToSort = False
                LrSort()
            End If
        Catch ex As NullReferenceException
        End Try
    End Sub

    ''' <summary>
    ''' The predicate used to filter the Letter Rip list.
    ''' </summary>
    Private Function LrFilter(ByVal item As Object) As Boolean
        Dim s As String = CStr(item)

        Dim wChars As New List(Of Char)(LrTextBox.Text.ToCharArray)

        For Each letter In s
            If Not wChars.Remove(letter) Then Return False
        Next

        Return True
    End Function

    Private Sub LrSort()
        Try
            If LrSortAscendingCheckBox.IsChecked Then
                If LrSortLengthCheckBox.IsChecked Then
                    wordList3 = New List(Of String) _
                        (From x In wordList3 Select x _
                         Order By x.Length Ascending)
                Else
                    wordList3 = New List(Of String) _
                        (From x In wordList3 Select x _
                         Order By x Ascending)
                End If
            Else
                If LrSortLengthCheckBox.IsChecked Then
                    wordList3 = New List(Of String) _
                        (From x In wordList3 Select x _
                         Order By x.Length Descending)
                Else
                    wordList3 = New List(Of String) _
                        (From x In wordList3 Select x _
                         Order By x Descending)
                End If
            End If

            LetterRipListBox.ItemsSource = wordList3
            LrGroup()
            'LetterRipListBox.Items.Refresh
        Catch ex As NullReferenceException
        End Try
    End Sub

    Private lexDsc As New LexicographicalGroupDescription()
    Private lenDsc As New LengthGroupDescription()

    Private Sub LrGroup()
        If LrGroupCheckBox.IsChecked Then
            If LrSortLengthCheckBox.IsChecked Then
                If Not (LetterRipListBox.Items.GroupDescriptions.Count = 0 _
                        OrElse LetterRipListBox.Items.GroupDescriptions(0) _
                            .Equals(lenDsc)) Then
                    LetterRipListBox.Items.GroupDescriptions.Clear()
                End If

                If LetterRipListBox.Items.GroupDescriptions.Count = 0 Then
                    LetterRipListBox.Items.GroupDescriptions.Add(lenDsc)
                End If
            Else
                If Not (LetterRipListBox.Items.GroupDescriptions.Count = 0 _
                        OrElse LetterRipListBox.Items.GroupDescriptions(0) _
                           .Equals(lexDsc)) Then
                    LetterRipListBox.Items.GroupDescriptions.Clear()
                End If

                If LetterRipListBox.Items.GroupDescriptions.Count = 0 Then
                    LetterRipListBox.Items.GroupDescriptions.Add(lexDsc)
                End If
            End If
        Else
            LetterRipListBox.Items.GroupDescriptions.Clear()
        End If

        'LetterRipListBox.Items.Filter = LetterRipListBox.Items.Filter
    End Sub

    Private Class LexicographicalGroupDescription
        Inherits GroupDescription

        Public Overrides Function GroupNameFromItem( _
                ByVal item As Object, ByVal level As Integer, _
                ByVal culture As CultureInfo) As Object
            If level = 0 Then Return Char.ToUpperInvariant(item.ToString.Chars(0)) _
                Else Return ""
        End Function

    End Class

    Private Class LengthGroupDescription
        Inherits GroupDescription

        Public Overrides Function GroupNameFromItem( _
                ByVal item As Object, ByVal level As Integer, _
                ByVal culture As Globalization.CultureInfo) As Object
            If level = 0 Then
                If item.ToString.Length = 1 Then
                    Return item.ToString.Length.ToString + " letter"
                Else
                    Return item.ToString.Length.ToString + " letters"
                End If
            End If
            Return ""
        End Function

    End Class

#End Region

End Class
