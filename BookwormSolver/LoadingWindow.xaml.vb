﻿Imports System.ComponentModel
Imports System.IO

Partial Public Class LoadingWindow

    Private WithEvents wordLoadBW As New BackgroundWorker
    Private Const wordListFile As String = "wordlist.txt"

    Friend Sub LoadWords()
        wordLoadBW.RunWorkerAsync()
    End Sub

    Private Sub wordLoadBW_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles wordLoadBW.DoWork
        ' Dim wordListEnc() As String = File.ReadAllLines(wordListFile)
        ' e.Result = BookwormListEncoder.Decode(wordListEnc.ToList)
        Using f As StreamReader = File.OpenText(wordListFile)

            Dim currentWord As String = f.ReadLine
            Dim line As String = ""
            Dim numberPref As Integer       ' The prefix of the word
            Dim numberPrefLast As Integer   ' The prefix of the last word.
            Dim ret As New List(Of String)
            Dim numberLen As Integer
            Dim nTmp As Integer
            Dim digitLength As Integer      ' The length of a digit in the prefix.
            Dim hasPrefix As Boolean = False

            ret.Add(currentWord)
            line = f.ReadLine
            Do
                If e.Cancel Then Exit Do

                ' Determine the number prefix
                For numberLen = 0 To line.Length - 1
                    If Not Integer.TryParse(line(numberLen), nTmp) Then
                        Exit For
                    End If
                Next
                If numberLen = 0 Then
                    ' Prefix was not found, inherit prefix from last word
                    numberPref = numberPrefLast
                    hasPrefix = False
                Else
                    numberPref = Integer.Parse(line.Substring(0, numberLen))
                    hasPrefix = True
                End If

                ' Process
                digitLength = IIf(hasPrefix, numberPref.ToString( _
                    Globalization.CultureInfo.InvariantCulture.NumberFormat).Length, _
                    0)
                Dim newWord1 As String = currentWord.Substring(0, numberPref)
                Dim newWord2 As String = line.Substring(digitLength, line.Length - digitLength)
                currentWord = newWord1 & newWord2
                ret.Add(currentWord)

                numberPrefLast = numberPref

                line = f.ReadLine
            Loop Until line Is Nothing

            e.Result = ret
        End Using
    End Sub

    Private Sub wordLoadBW_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles wordLoadBW.RunWorkerCompleted
        If e.Cancelled Then
            Dim res = MessageBox.Show("Loading cancelled. OK to restart loading.", _
                    "Bookworm Solver - loading", _
                    MessageBoxButton.OKCancel, _
                    MessageBoxImage.Asterisk)
            If res = MessageBoxResult.Cancel Then
                RaiseEvent LoadingFinished(New List(Of String), True)
            End If
        Else
            RaiseEvent LoadingFinished(CType(e.Result, List(Of String)), False)
        End If
    End Sub

    Friend Event LoadingFinished(ByVal words As List(Of String), ByVal doExit As Boolean)

    Private Sub LoadingWindow_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Input.KeyEventArgs) Handles Me.KeyDown
        If e.Key = Key.Escape Then
            wordLoadBW.CancelAsync()
        End If
    End Sub
End Class
